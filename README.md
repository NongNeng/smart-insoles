
## How to build and run

Build with ionic
```
ionic cordova build android
```

Or we can build and run
```
ionic cordova run android
```

Check that device is connected with usb debug
```
adb devices -l
```

Uninstall with usb connected
```
adb uninstall io.smarts.insoles
```

Install in device
```
adb install platforms/android/build/outputs/apk/android-debug.apk
```

Run program
```
adb shell -p "io.smarts.insoles" -c android.intent.category.LAUNCHER 1
```


