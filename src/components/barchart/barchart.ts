import { Component, ElementRef } from '@angular/core';
import { NgZone, Input, ViewChild } from '@angular/core';
import * as d3 from "d3";
import {Converter} from "../../services/converter";
import {InsolesProvider} from "../../providers/insoles";

/**
 * Generated class for the BarchartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'barchart',
    templateUrl: 'barchart.html'
})
export class BarchartComponent {

    text: string;
    @Input('slideId') slideId: number;
    @Input('currentIndex') currentIndex: number;
    @Input('isLeft') isLeft: boolean;

    @ViewChild('chart') svg: ElementRef;
    constructor(
        private insolesData: InsolesProvider
    ) {

    }

    @ViewChild('container') container: ElementRef;

    ngAfterViewInit() {

        let maxADC = 607;

        let container_height = this.container.nativeElement.clientHeight;
        let container_width = this.container.nativeElement.clientWidth;
        var margin = {top: 20, right: 20, bottom: 50, left: 40},
            width = container_width - margin.left - margin.right,
            height = container_height - margin.top - margin.bottom;

        var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
            y = d3.scaleLinear().rangeRound([height, 0]);

        let svg = d3.select(this.svg.nativeElement);

        svg.attr("width", width ).attr("height", height);

        var g = svg.append("g")
            .attr("transform", "translate(" + 0 + ", " +0+ ")");

        let data  = [
            {"sensors":"A","value":0},
            {"sensors":"B","value":0},
            {"sensors":"C","value":0},
            {"sensors":"D","value":0},
            {"sensors":"E","value":0},
            {"sensors":"F","value":0},
            {"sensors":"G","value":0},
            {"sensors":"H","value":0},
        ];

        x.domain(data.map(function(d) { return d.sensors; }));
        y.domain([0, toKilogram(maxADC) ]);

        g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y).ticks(10))
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .text("Value");

        if( data.length > 0 ){

            d3.timer(()=> {

                if( this.currentIndex == this.slideId ){

                    let sensors = [7];
                    if(this.isLeft){
                        sensors = this.insolesData.getDataLeft().sensors;
                    }else{
                        sensors = this.insolesData.getDataRight().sensors;
                    }

                    let data  = [
                        {"sensors":"A","value":toKilogram(sensors[0])},
                        {"sensors":"B","value":toKilogram(sensors[1])},
                        {"sensors":"C","value":toKilogram(sensors[2])},
                        {"sensors":"D","value":toKilogram(sensors[3])},
                        {"sensors":"E","value":toKilogram(sensors[4])},
                        {"sensors":"F","value":toKilogram(sensors[5])},
                        {"sensors":"G","value":toKilogram(sensors[6])},
                        {"sensors":"H","value":toKilogram(sensors[7])},
                    ];

                    g.selectAll(".bar")
                        .remove()
                        .exit()
                        .data(data)
                        .enter().append("rect")
                        .attr("class", "bar")
                        .attr("width", x.bandwidth())
                        .attr("x", function(d) { return x(d.sensors); })
                        .attr("y", function(d) { return y(d.value); })
                        .attr("height", function(d) { return height - y(d.value); });

                    g.selectAll(".value")
                        .remove()
                        .exit()
                        .data(data)
                        .enter().append("text")
                        .attr("class", "value")
                        .attr("text-anchor", "middle")
                        .attr("x", function(d) { return x(d.sensors) + (x.bandwidth()/2); })
                        .attr("y", function(d) { return y(d.value) - 10; })
                        .text(function(d) { return d.value.toFixed(2) });

                }

            });
        }

    }
}

function toKilogram(x) {
    if(x == 0){
        return 0;
    }
    return (new Converter).toKilogram(x);
}