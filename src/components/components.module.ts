import { NgModule } from '@angular/core';
import { InsoleComponent } from './insole/insole';
import {IonicModule} from "ionic-angular";
import { BarchartComponent } from './barchart/barchart';
import { WeightShiftComponent } from './weight-shift/weight-shift';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DecimalPipe } from "@angular/common";

@NgModule({
    imports: [
        NgxChartsModule,
        IonicModule.forRoot(InsoleComponent),
    ],
    declarations: [
        InsoleComponent,
        WeightShiftComponent,
        BarchartComponent
    ],
    providers: [ DecimalPipe ],
    exports: [
        InsoleComponent,
        WeightShiftComponent,
        NgxChartsModule,
        BarchartComponent
    ]
})
export class ComponentsModule {}
