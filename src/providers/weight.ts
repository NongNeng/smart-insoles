
import {Injectable} from "@angular/core";
import {InsolesProvider} from "../providers/insoles";
import {Converter} from "../services/converter";
import {Observable} from "rxjs/Observable";


@Injectable()
export class WeightProvider {

  get data(): Object {
    return this._data;
  }

  public dataObservable :Observable<any>;
  private _data: Object = {};
  private emitter : any;

  constructor(
    private insolesData: InsolesProvider
  ){

      this._data['sum_l'] = 0;
      this._data['max_l'] = 0;
      this._data['avg_l'] = 0;
      this._data['sum_r'] = 0;
      this._data['max_r'] = 0;
      this._data['avg_r'] = 0;
      this._data['sum_all'] = 0;

      this.dataObservable = Observable.create(e => this.emitter = e);
      this.dataObservable.subscribe((data)=>{
          //console.log('WeightProvider',data);
      });

      this.insolesData.dataObservable.subscribe((data)=>{
        //console.log('WeightProvider','insolesData.dataObservable.subscribe');
        let left = data.left;
        this._data['max_l'] = toKilogram( left.max );
        this._data['avg_l'] = toKilogram( left.avg );

        let right = data.right;
        this._data['max_r'] = toKilogram( right.max,false );
        this._data['avg_r'] = toKilogram( right.avg,false );


        let sum_adc_l =
          left.sensors[0] +
          left.sensors[1] +
          left.sensors[2] +
          left.sensors[3] +
          left.sensors[4] +
          left.sensors[5] +
          left.sensors[6] +
          left.sensors[7];

        this._data['sum_l'] = toKilogram(sum_adc_l);

          let sum_adc_r =
          right.sensors[0] +
          right.sensors[1] +
          right.sensors[2] +
          right.sensors[3] +
          right.sensors[4] +
          right.sensors[5] +
          right.sensors[6] +
          right.sensors[7];

        this._data['sum_r'] = toKilogram(sum_adc_r,false);

        this._data['sum_all'] = this._data['sum_l'] + this._data['sum_r'];

        this.emitter.next(this._data);

      });
  }
}

function toKilogram(x,isLeft=true) {
  if( x==0 ){
    return 0;
  }
  return (new Converter()).toKilogram(x,isLeft);
}
