import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as d3 from "d3";
import {BLE} from "@ionic-native/ble";
import {ActionSheetController} from "ionic-angular";
import {Converter} from "../services/converter";
import "rxjs/add/operator/mergeMap";
import {Observable} from "rxjs/Observable";

function toKilogram(x) {
    return (new Converter).toKilogram(x);
}
function bytesToString (buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

const average = arr => arr.reduce( ( p, c ) => parseInt(p) + parseInt(c), 0 ) / arr.length;

@Injectable()
export class InsolesProvider {

  devices: Array<any> =  [];
  connected: Array<any> = [];
  data : any = {};
  public dataObservable :Observable<any>;
  private emitter : any;

  constructor(
      private ble: BLE,
      private actionSheetCtrl: ActionSheetController
  ) {

      let sensors = [0,0,0,0,0,0,0,0]
      this.data.left = {};
      this.data.left.sensors = sensors;
      this.data.left.max = 0;
      this.data.left.avg = 0;

      this.data.right = {};
      this.data.right.sensors = sensors;
      this.data.right.max = 0;
      this.data.right.avg = 0;

      this.dataObservable = Observable.create(e => this.emitter = e);
      this.dataObservable.subscribe((data)=>{
        //console.log('InsolesProvider',data);
      });

  }

  updateData( sensors , is_left) {

    //console.log('sensors',is_left,sensors);

    if(is_left){

        this.data.left = {};
        this.data.left.sensors = sensors;
        this.data.left.max = Math.max.apply(null,sensors );
        this.data.left.avg = average( sensors );

    } else {

        this.data.right = {};
        this.data.right.sensors = sensors;
        this.data.right.max = Math.max.apply(null,sensors);
        this.data.right.avg = average( sensors );

    }

    this.emitter.next(this.data);

  }

    addDevice(device){
        if(typeof this.devices.find(myObj => myObj.id == device.id) == 'undefined'){
            this.devices.push(device)
        }
    }
    removeDevice(device){
        let devices_index = this.devices.findIndex(x => x.id === device.id)
        if(-1 !== devices_index) {
            this.devices.splice(devices_index, 1);
        }
    }
    addConnectedDevice(device){
        if(typeof this.connected.find(myObj => myObj.id == device.id) == 'undefined'){
            this.connected.push(device)
        }
    }
    removeConnectedDevice(device){
        let devices_index = this.connected.findIndex(x => x.id === device.id)
        if(-1 !== devices_index) {
            this.connected.splice(devices_index, 1);
        }
    }

  connect( id, is_left , success , error ){

      this.ble.connect(id).subscribe( device => {

          device.is_left = is_left;
          console.log("connected is left", is_left, device ,'characteristics' , device.characteristics )
          console.log(device.characteristics);

          let subscribeAble = [];

          for( let character of device.characteristics ) {
              if( -1 !== character.properties.indexOf("Notify") ){
                  console.log('Able to subscribe',character);
                  subscribeAble.push(character);
              }
          }

          if( subscribeAble.length > 1 ){

              let buttons = [];
              for( let character of subscribeAble ) {
                  buttons.push(
                      {
                          text: 'character:' + character.characteristic + ' service:' + character.service ,
                          handler: () => {
                              this.startNotification(device,character,error);
                          }
                      }
                  );
              }

              buttons.push({
                  text: 'Cancel',
                  role: 'destructive',
                  handler: () => {
                      this.ble.disconnect(device.id);
                      console.log('Cancel click');
                  }
              });

              let actionSheet = this.actionSheetCtrl.create({
                  title: 'Select which characteristic on service to subscribe for ' + device.name ? device.name : device.id ,
                  buttons: buttons
              });
              actionSheet.present()
          }

          if( subscribeAble.length == 1 ){
              let character = subscribeAble[0];
              this.startNotification(device,character,error);

          }

          success(device);

      } , e => {
          if( e == "cordova_not_available" ){
              let device = {id: `id`, name: `Device connected`,'is_left':is_left}
              this.removeDevice(device);
              this.addConnectedDevice(device);
              success(device);


              this.mockData();
              setInterval( ()=>{ this.mockData() },100 );

          } else {
              error(e)
          }
      } );

  }

  disconnect(device,success){

      console.log('is_left',device.is_left);
      console.log('device.subscribe',device.subscribe);

      this.ble.isConnected(device.id).then(()=>{
          this.ble.stopNotification(device.id, device.subscribe.service, device.subscribe.characteristic).then(()=>{
              this.ble.disconnect(device.id).then(()=>{
                  this.removeConnectedDevice(device);
                  device.is_left = null;
                  this.addDevice(device);
                  if(success) success(device);
              });
          });
      },reason => {
          if( reason == "cordova_not_available" ){
              device.name = "Name";
              device.is_left = null;
              this.removeConnectedDevice(device);
              this.addDevice(device);
              if(success) success(device);
          }
      });
  }

  startNotification(device,character,error) {

      this.removeDevice(device);
      device.subscribe = character;
      this.addConnectedDevice(device);

      let chunk = ``;
      let complete = ``;

      this.ble.startNotification( device.id , character.service , character.characteristic )
        .subscribe( ArrayBuffer => {

            let data = bytesToString(ArrayBuffer);
            chunk += data;

            const regex = /([\d,]+\|)/g;

            let matches = regex.exec(chunk);
            complete = ``;

            if(matches){

                complete = matches[1].replace(/\|/,'');
                chunk = chunk.replace(/.+\|/,'');
                let arr = [];
                arr.push.apply(arr, complete.split(",").map(Number));

                if(device.is_left){
                    this.updateData( arr , device.is_left);
                } else {
                    this.updateData( arr , device.is_left);
                }

            }

        },(e)=>{

            this.removeConnectedDevice(device);
            this.addDevice(device);
            this.ble.disconnect(device.id);
            console.log(e)
            error(e)

        });
  }

  getDataLeft() {

    return this.data.left;

  }

  getDataRight() {
      return this.data.right;
  }

  mockData(){

      let ran0 = d3.randomUniform( 550 , 607 )();
      let ran1 = d3.randomUniform( 555 , 607 )();
      let ran2 = d3.randomUniform( 535 , 607 )();
      let ran3 = d3.randomUniform( 550 , 607 )();
      let ran4 = d3.randomUniform( 550 , 607 )();
      let ran5 = d3.randomUniform( 555 , 607 )();
      let ran6 = d3.randomUniform( 535 , 607 )();
      let ran7 = d3.randomUniform( 550 , 607 )();

      let sensors = [ran0,ran1,ran2,ran3,ran4,ran5,ran6,ran7];

      this.data.left = {};
      this.data.left.sensors = sensors;
      this.data.left.max = toKilogram(Math.max.apply(null,sensors ));
      this.data.left.avg = toKilogram(average( sensors ));

      this.data.right = {};
      this.data.right.sensors = sensors;
      this.data.right.max = this.data.left.max
      this.data.right.avg = this.data.left.avg
      this.emitter.next(this.data);
  }

}
