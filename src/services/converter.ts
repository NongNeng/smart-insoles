
import {Injectable} from "@angular/core";

@Injectable()
export class Converter {

    left_a: number =  2.39965;
    right_a: number = 2.1284;
    avg_a: number = (2.39965 + 2.1284) / 2;

    constructor() {

    }

    toKilogram(x,isLeft=true) {

        if(x == 0){
            return 0;
        }
        if(isLeft){
          return this.left(x);
        }
        return this.right(x);
    }

    private  right(x) {
      let a = this.right_a;
      // let a = this.avg_a;
      let b = 1.0008;
      return a*Math.pow(b,x);
    }
    private  left(x) {
      let a = this.left_a;
      // let a = this.avg_a;
      let b = 1.0008;
      return a*Math.pow(b,x);
    }
}
