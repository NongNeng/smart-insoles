import { Component, NgZone } from '@angular/core';
import { LoadingController, ActionSheetController, NavController} from 'ionic-angular';
import { InsolesProvider } from "../../providers/insoles";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    constructor(
                private zone: NgZone,
                public navCtrl: NavController,
                public loadingCtrl: LoadingController,
                private insoles: InsolesProvider,
                public actionSheetCtrl: ActionSheetController
    )
    {

    }

    connectSelected = function(device) {
        this.connect(device.id);
    }

    connect(id) {

        let actionSheetLR = this.actionSheetCtrl.create({
            title: 'Select side of this insole',
            buttons: [
                {
                    text: 'Left',
                    handler: () => {
                        this.selectedSide(id,true);
                        console.log('this Left');
                    }
                },
                {
                    text: 'Right',
                    handler: () => {
                        this.selectedSide(id,false);
                        console.log('this Right');
                    }
                },
                {
                    text: 'Cancel',
                    role: 'destructive',
                    handler: () => {
                        console.log('Cancel');
                    }
                }
            ]
        });

        actionSheetLR.present();

    }

    selectedSide( id, is_left = true ) {

        let loader = this.loadingCtrl.create({
            content: "Connecting please wait..."
        });

        loader.present().then(()=>{

            this.insoles.connect( id, is_left, () => {
                loader.dismiss();
            } , error => {
                console.log("error is left " , is_left ,error);
                loader.dismiss();
            });

        });

    }

    clickConnected = function(device) {

        let actionSheetClickConnected = this.actionSheetCtrl.create({
            title: 'Do you want to disconnect this insole?',
            buttons: [
                {
                    text: 'Yes',
                    role: 'destructive',
                    handler: () => {
                        this.disconnectSelected(device);
                        console.log('disconnectSelected');
                    }
                }
            ]
        });

        actionSheetClickConnected.present();

    }

    disconnectSelected = function(device) {

        console.log('device',device);

        this.zone.run(() => {
            this.insoles.disconnect(device);
        });


    }

    startScan = function(){

        console.log('startScan');

        this.insoles.ble.enable();

        this.insoles.devices = [];
        this.insoles.ble.scan([],5).subscribe(
            device => {
                console.log(JSON.stringify(device));
                this.zone.run(() => { //running inside the zone because otherwise the view is not updated
                    this.insoles.addDevice(device);
                });
            },
            err => {
                console.log("cordova_not_available",JSON.stringify(err));
                if( err == "cordova_not_available"){
                    console.log('Show mockup data');
                    this.zone.run(() => {
                        let device = {};
                        device['id'] = 'id'
                        device['name'] = 'Name'
                        this.insoles.addDevice(device)
                    });
                }
            });

    }

  goToContourPage = function() {

    this.navCtrl.push('InsolesPage');

  }

  devicesHeaderFn(record, recordIndex, records) {
        if (recordIndex === 0 && records.length > 0 ) {
            return recordIndex;
        }
        return null;
  }

}
