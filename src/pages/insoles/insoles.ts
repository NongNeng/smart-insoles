import {Component, NgZone, ViewChild} from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {InsolesProvider} from "../../providers/insoles";
import { Slides } from 'ionic-angular';
import { WeightProvider } from "../../providers/weight";

/**
 * Generated class for the InsolesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-insoles',
  templateUrl: 'insoles.html'
})
export class InsolesPage {

    private left_max: number = 0;
    private left_avg: number = 0;

    private right_max: number = 0;
    private right_avg: number = 0;

    private currentIndex: number = 0;

    private sum: number = 0;

    @ViewChild(Slides) slides: Slides;

    constructor(
        private zone: NgZone,
        private insolesData: InsolesProvider,
        private weight: WeightProvider
    ) {

    }

    ionViewDidLoad() {

        this.zone.run(() => {
          this.weight.dataObservable.subscribe((data)=>{
            this.left_max = data.max_l;
            this.left_avg = data.avg_l;
            this.right_max = data.max_r;
            this.right_avg = data.avg_r;
            this.sum = data.sum_all;
          });
        });

    }

    slideChanged() {
        this.currentIndex = this.slides.getActiveIndex();
    }


}
