import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsolesPage } from './insoles';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    InsolesPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(InsolesPage),
  ],
})
export class InsolesPageModule {
  
}
